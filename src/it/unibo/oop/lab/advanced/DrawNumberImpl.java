package it.unibo.oop.lab.advanced;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Random;
import java.util.StringTokenizer;

import it.unibo.oop.lab.iogui.BadIOGUI;

/**
 *
 */
public final class DrawNumberImpl implements DrawNumber {

    private int choice;
    private final int min;
    private final int max;
    private final int attempts;
    private int remainingAttempts;
    private final Random random = new Random();
    private final String s = System.getProperty("file.separator");
    private final String PATH = "System.getProperty(\"user.home\")" + "Gianfree" + s + "Google Drive" + s + "Università" +
            s + "2 ANNO" + s + "Programmazione Ad Oggetti" + s + "Laboratorio" + s + "lab08" + s + "lab-08" + s + "res" + s + "settigs" + s + "config.yml";
    private final File config;

    /**
     * @param min
     *            minimum number
     * @param max
     *            maximum number
     * @param attempts
     *            the maximum number of attempts
     * @throws IOException 
     */
    public DrawNumberImpl(final int min, final int max, final int attempts) throws IOException {
        
        
        config = new File(PATH);
        
        try (
             FileReader fileReader = new FileReader(config);
             BufferedReader bufferFile = new BufferedReader(fileReader)
             ) {
                 ArrayList<Integer> result = new ArrayList<>();
                 List<String> lines = Files.readAllLines(PATH, "ISO-8859-1");
            for (String line : lines) {
                if(line.contains(Number)) {
                    StringTokenizer token = new StringTokenizer(line, ":");
                    result.add(Integer.parseInt(token.nextToken()));
                }
            }
        }
        
        this.min = min;
        this.max = max;
        this.attempts = attempts;
        this.reset();
    }
/*
    * Use a ``FileReader`` inside a ``BufferedReader`` to get the file content. Another option is ``Files.readAllLines()``. 
    * The first way is described in detail on the slides, the second method must be learnt from the Javadoc.

    * The file (see the example in the "res" folder) is a [standard YAML file](https://en.wikipedia.org/wiki/YAML). 
    * There are libraries to parse YML easily, but this very file is so simple that no particular strategy is required: just filter what is before and after the colon.

    * Do not make the View operate on the file system. 
    * More precisely, the controller in its constructor must be able to read the provided configuration file, and import the three constants.
    *  Even better, an external utility (either a method or another class) could be responsible for that. 
    *  The reason is that the interaction with the file system is not part of the domain model, nor, in this case, is part of the view.

    * You can use ``StringTokenizer`` to split a ``String`` in parts. 
    * Another option, available for this very case, is ``String.split()``, passing ``":"`` as argument. Beware of ``split()``: the input ``String`` is actually a regular expression. 
    * Regular expressions (regex) are a very powerful tool, but we can not cover them in this course: ``StringTokenizer`` is recommended unless you already know regex.
    
    
    
   */ 
    
    
    @Override
    public void reset() {
        this.remainingAttempts = this.attempts;
        this.choice = this.min + random.nextInt(this.max - this.min + 1);
    }

    @Override
    public DrawResult attempt(final int n) throws AttemptsLimitReachedException {
        if (this.remainingAttempts <= 0) {
            throw new AttemptsLimitReachedException();
        }
        if (n < this.min || n > this.max) {
            throw new IllegalArgumentException("The number is outside boundaries");
        }
        remainingAttempts--;
        if (n > this.choice) {
            return DrawResult.YOURS_HIGH;
        }
        if (n < this.choice) {
            return DrawResult.YOURS_LOW;
        }
        return DrawResult.YOU_WON;
    }

}
