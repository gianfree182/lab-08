package it.unibo.oop.lab.mvc;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Arrays;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI extends ControllerImpl {

    private final JFrame frame = new JFrame();
    final static float SIZEFONT = 50;
    private ControllerImpl controller;

    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application *OK
     * 
     * 2) In its constructor, sets up the whole view *OK
     * 
     * 3) The graphical interface consists of a JTextField in the upper part of the frame, 
     * a JTextArea in the center and two buttons below it: "Print", and "Show history". 
     * SUGGESTION: Use a JPanel with BorderLayout *OK
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation) *OK
     * 
     * 5) The behavior of the program is that, if "Print" is pressed, the
     * controller is asked to show the string contained in the text field on standard output. 
     * If "show history" is pressed instead, the GUI must show all the prints that
     * have been done to this moment in the text area.
     * 
     * 
     * 
     * 
     * 
     */

    /**
     * builds a new {@link SimpleGUI}.
     */
    public SimpleGUI() {
        
        this.controller = new ControllerImpl();
        JPanel mainPanel = new JPanel();
        mainPanel.setLayout(new BorderLayout());
        
        this.frame.getJMenuBar();
        this.frame.getDefaultCloseOperation();
        this.frame.getContentPane().add(mainPanel);
        this.frame.setTitle("Simple controller responsible of I/O access");
        
        JTextField textField = new JTextField();
        mainPanel.add(textField, BorderLayout.NORTH);
        textField.setFont(textField.getFont().deriveFont(SIZEFONT));
        
        
        JTextArea textArea = new JTextArea();
        textArea.setFont(textArea.getFont().deriveFont(SIZEFONT));
        mainPanel.add(textArea, BorderLayout.CENTER);
        
        JPanel panelForButton = new JPanel();
        panelForButton.setLayout(new BorderLayout());
        mainPanel.add(panelForButton,BorderLayout.SOUTH);
        
        JButton printButton = new JButton();
        printButton.setText("Print");
        printButton.setFont(printButton.getFont().deriveFont(SIZEFONT));
        
        printButton.addActionListener(new ActionListener () {
            public void actionPerformed(final ActionEvent e) {
                addPrintInHistory(textField.getText());
                setCurrentPrint(textField.getText());
                System.out.println(getCurrentPrint()); 
            }
        }
        );
        
    
        JButton showHistoryButton = new JButton();
        showHistoryButton.setText("Show History");
        showHistoryButton.setFont(showHistoryButton.getFont().deriveFont(SIZEFONT));
       
        showHistoryButton.addActionListener(new ActionListener() {
           public void actionPerformed (final ActionEvent a) {
               textArea.setText(getHistoryPrint());
               System.out.println(getHistoryPrint());
           }
       });
        
        panelForButton.add(printButton, BorderLayout.WEST);
        panelForButton.add(showHistoryButton, BorderLayout.EAST);
        
        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);

        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */
        frame.setLocationByPlatform(true);
    }
    
    public static void main (String [] args) {
        SimpleGUI gui = new SimpleGUI();
        gui.frame.setVisible(true);
    }

}
