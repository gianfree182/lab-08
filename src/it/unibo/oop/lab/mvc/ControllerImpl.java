package it.unibo.oop.lab.mvc;


import java.util.LinkedList;
import java.util.Queue;

public class ControllerImpl implements Controller {

    private String currentPrint;
    private Queue<String> historyPrint;
    
    public ControllerImpl () {
        this.currentPrint = "";
        historyPrint = new LinkedList<String>();
    }
     
      
    public String getCurrentPrint() {
        return currentPrint;
    }

    public String getHistoryPrint() {
        return historyPrint.toString();
    }
    
    public void addPrintInHistory (String string) {
        this.historyPrint.add(string);
    }

    public void setCurrentPrint(String string) {
        this.currentPrint = string;
    }

    @Override
    public void setPrint(String text) throws NullPointerException {
    
        setCurrentPrint(text);
        // throw new NullPointerException();
        addPrintInHistory(text);
    }

    @Override
    public String getNextPrint() {
        return this.getHistoryPrint().peek();
    }

    @Override
    public Queue<String> getHistory() {
        return getHistoryPrint();
    }

    @Override
    public void printCurrentPrint() {
        System.out.println(this.currentPrint);
    }

}
