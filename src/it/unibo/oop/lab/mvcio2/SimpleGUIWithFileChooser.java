package it.unibo.oop.lab.mvcio2;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

import it.unibo.oop.lab.mvcio.SimpleGUI;

/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUIWithFileChooser {
    
    public static SimpleGUI simpleGui;
    final static float SIZEFONT = 50;
    
    public SimpleGUIWithFileChooser () throws IOException {

        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        
        simpleGui = new SimpleGUI();
        simpleGui.getFrame().setTitle("My second java graphical interface");
        
        final JTextField textField = new JTextField();
        String result = simpleGui.getController().getPathCurrentFile();
        textField.setText(result);
        textField.setFont(textField.getFont().deriveFont(SIZEFONT));
        textField.setEditable(false);
        
        final JButton browseButton = new JButton();
        browseButton.setText("Browse...");
        browseButton.setFont(browseButton.getFont().deriveFont(SIZEFONT));
        
        final JPanel secondPanel = new JPanel();
        secondPanel.setLayout(new BorderLayout());
        secondPanel.add(browseButton, BorderLayout.EAST);
        secondPanel.add(textField);
        
        simpleGui.getFrame().getContentPane().add(secondPanel, BorderLayout.NORTH);
     
        browseButton.addActionListener(new ActionListener () {

            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser chooser = new JFileChooser ();
                int ris = chooser.showSaveDialog(simpleGui.getFrame());
                
                if(chooser.APPROVE_OPTION == ris) {
                    try {
                        simpleGui.getController().setCurrentFile(chooser.getSelectedFile());
                        //modo per mettere il testo della textarea nel current file
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                } else {
                    JTextField errorMessage = new JTextField();
                    errorMessage.setText("An error occurred. Try again.");
                    errorMessage.setFont(errorMessage.getFont().deriveFont(SIZEFONT));
                    errorMessage.setEditable(false);
                    JOptionPane.showMessageDialog(chooser, errorMessage);
                }
                //a me nel file output.dat salva il path del file che seleziono
                //nel browse
            }      
           });
             
     
    }

    public static void main(final String[] args) throws IOException {
        final SimpleGUIWithFileChooser guiAdvance = new SimpleGUIWithFileChooser();
        guiAdvance.simpleGui.getFrame().setVisible(true);
    }
    

    /*
     * TODO: Starting from the application in mvcio:
     * 
     * 1) Add a JTextField and a button "Browse..." on the upper part of the
     * graphical interface.
     * Suggestion: use a second JPanel with a second BorderLayout, put the panel
     * in the North of the main panel, put the text field in the center of the
     * new panel and put the button in the line_end of the new panel.
     * 
     * 2) The JTextField should be non modifiable. And, should display the
     * current selected file.
     * 
     * 3) On press, the button should open a JFileChooser. The program should
     * use the method showSaveDialog() to display the file chooser, and if the
     * result is equal to JFileChooser.APPROVE_OPTION the program should set as
     * new file in the Controller the file chosen. If CANCEL_OPTION is returned,
     * then the program should do nothing. Otherwise, a message dialog should be
     * shown telling the user that an error has occurred (use
     * JOptionPane.showMessageDialog()).
     * 
     * 4) When in the controller a new File is set, also the graphical interface
     * must reflect such change. Suggestion: do not force the controller to
     * update the UI: in this example the UI knows when should be updated, so
     * try to keep things separated.
     */

}
