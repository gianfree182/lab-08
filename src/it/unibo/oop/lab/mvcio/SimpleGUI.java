package it.unibo.oop.lab.mvcio;

import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileNotFoundException;
import java.io.IOException;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTextArea;


/**
 * A very simple program using a graphical interface.
 * 
 */
public final class SimpleGUI {
    
    final static float SIZEFONT = 50;
    private final  JFrame frame = new JFrame();
    private final Controller controller;

    public JFrame getFrame () {
        return this.frame;
    }

    public Controller getController () {
        return this.controller;
    }
    /*
     * Once the Controller is done, implement this class in such a way that:
     * 
     * 1) I has a main method that starts the graphical application
     * 
     * 2) In its constructor, sets up the whole view
     * 
     * 3) The graphical interface consists of a JTextArea with a button "Save"
     * right below (see "ex02.png" for the expected result). SUGGESTION: Use a
     * JPanel with BorderLayout
     * 
     * 4) By default, if the graphical interface is closed the program must exit
     * (call setDefaultCloseOperation)
     * 
     * 5) The behavior of the program is that, if "Save" is pressed, the
     * controller is asked to save the file.
     * 
     * Use "ex02.png" (in the res directory) to verify the expected aspect.
     */



    /**
     * builds a new {@link SimpleGUI}.
     * @throws IOException 
     */
       public SimpleGUI() throws IOException {

       controller = new Controller();
       frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
       frame.setTitle("My first java graphical interface");

       final JPanel panel = new JPanel();
       panel.setLayout(new BorderLayout());
       frame.getContentPane().add(panel, BorderLayout.CENTER);

       final JButton saveButton = new JButton();
       saveButton.setText("Save");
       saveButton.setFont(saveButton.getFont().deriveFont(SIZEFONT));
       
      
       final JPanel canvas = new JPanel();
       canvas.setLayout(new BorderLayout());
       canvas.add(saveButton, BorderLayout.SOUTH);

       final JTextArea textArea = new JTextArea();
       textArea.setFont(textArea.getFont().deriveFont(SIZEFONT));
       panel.add(canvas, BorderLayout.SOUTH);
       panel.add(textArea);
       
       saveButton.addActionListener(new ActionListener () {

           @Override
           public void actionPerformed(ActionEvent e) {
               try {
                   
                   controller.setCurrentFileToText(textArea.getText());
               } catch (FileNotFoundException e1) {
                   e1.printStackTrace();
               } catch (IOException e1) {
                
                e1.printStackTrace();
            }
               
               
           }
              
          });


        /*
         * Make the frame half the resolution of the screen. This very method is
         * enough for a single screen setup. In case of multiple monitors, the
         * primary is selected.
         * 
         * In order to deal coherently with multimonitor setups, other
         * facilities exist (see the Java documentation about this issue). It is
         * MUCH better than manually specify the size of a window in pixel: it
         * takes into account the current resolution.
         */
        final Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
        final int sw = (int) screen.getWidth();
        final int sh = (int) screen.getHeight();
        frame.setSize(sw / 2, sh / 2);
        /*
         * Instead of appearing at (0,0), upper left corner of the screen, this
         * flag makes the OS window manager take care of the default positioning
         * on screen. Results may vary, but it is generally the best choice.
         */

        frame.setLocationByPlatform(true);


    }

    public static void main(final String[] args) throws IOException {
        SimpleGUI simple = new SimpleGUI ();
        simple.frame.setVisible(true);
    }


}
