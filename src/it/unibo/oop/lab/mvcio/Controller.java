package it.unibo.oop.lab.mvcio;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.io.Serializable;

/**
 * 
 */
public class Controller implements Serializable  {
    /**
     * 
     */
    private static File output;
    private static final long serialVersionUID = -5114880985827690781L;
    private String PATH = System.getProperty("user.home") + 
            System.getProperty("file.separator") + "output.dat";
    
    public Controller () throws FileNotFoundException {
        output = new File(PATH);
        
    }
    
    public void setCurrentFile (File file) throws IOException {
       try( PrintWriter out = new PrintWriter(output) ) {
           out.print(file);
           output = file;
           
       }
    }
    
    public void setCurrentFileToText (String text) throws IOException {
        try(  PrintWriter out = new PrintWriter(PATH); ){
            out.print(text);
            File currentFile = new File(text);
            PATH = currentFile.getPath();
        }
    }
    
    public File getCurrentFile () {
        return output;
    }
    
    public String getPathCurrentFile () {
       
        return output.getPath();
    }
    
    public void setSerializable (File file) throws IOException {
       try (
               final OutputStream serial = new FileOutputStream(file);
               final OutputStream stream = new BufferedOutputStream(serial);
               final ObjectOutputStream serialStream = new ObjectOutputStream (stream);
               PrintStream out = new PrintStream(output);
            ) {
           out.println(serialStream); 
       } catch (IOException e) {
           System.out.println("Error. 404 Not Found");
       }
    }
    
    /*
     * This class must implement a simple controller responsible of I/O access. It
     * considers a single file at a time, and it is able to serialize objects in it.
     * 
     * Implement this class in such a way that:
     * 
     * 1) It has a method for setting a File as current file
     * 
     * 2) It has a method for getting the current File
     * 
     * 3) It has a method for getting the path (in form of String) of the current
     * File
     * 
     * 4) It has a method that gets a Serializable as input and saves such Object in
     * the current file. Remember how to use the ObjectOutputStream. This method may
     * throw IOException.
     * 
     * 5) By default, the current file is "output.dat" inside the user home folder.
     * A String representing the local user home folder can be accessed using
     * System.getProperty("user.home"). The separator symbol (/ on *nix, \ on
     * Windows) can be obtained as String through the method
     * System.getProperty("file.separator"). The combined use of those methods leads
     * to a software that run correctly on every platform.
     */

}
